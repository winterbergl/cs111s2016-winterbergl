
//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab #4
// Date: February 18, 2016
//
// Purpose: To make change for a customer
//*************************************
import java.util.Date;
import java.util.Scanner;

public class Lab4
{
    //----------------------------
    // main method: program execution begins here
    //----------------------------
    public static void main (String[] args)
    {
          //Label output with name and date:
        System.out.println("Lauren Winterberg\n Lab #4\n" + new Date() + "\n");

    String name;
    int tens; //number of ten dollar bills needed
    int fives; //number of five dollar bills needed
    int ones; //number of one dollar bills needed
    int cents = 7212; //value to make change for
    int quarters = 0; //number of quarters needed
    int nickels = 0; //number of nickels needed
    int dimes = 1; //number of dimes needed
    int pennies = 2; //number of pennies needed

    System.out.println("To make change for " + cents + "cents, you need:");

    tens = cents / 1000; // number of $10 bills contained in cents (quotient)
    cents = cents % 1000; // cents remaining after tens removed (remainder)

    System.out.print(tens + "tens, "); // Only use print so that output is generated together and not in chunks

    fives = cents / 500; // number of $5 dollar bills contained in cents (quotient)
    cents = cents % 500; // cents remaining after fives removed (remainder)

    System.out.print(fives + "fives, "); // so that output is printed in a group

    ones = cents / 100; // number of $1 bills contained in cents (quotient)
    cents = cents % 100; // cents remaining after ones removed (remainder)

    System.out.print(ones + "ones, "); // done so that output is printed in a group

    quarters = quarters / 1; // number of quarters contained in cents  (quotient)
    cents = cents % 1; // cents remaining after quarters removed (remainder)

    System.out.print(quarters + "quarters, "); // done so that output is printed in a group

    nickels = nickels / 1; // number of nickels contained in cents (quotient)
    cents = cents % 1; // cents remaining after nickels removed (remainder)

    System.out.print(nickels + "nickels, "); //done so that output is printed in a group

    dimes = dimes / 1; // number of dimes contained in cents (quotient)
    cents = cents % 1; // cents remaining after dimes removed (remainder)

    System.out.print(dimes + "dimes, "); // done so that output is printed in a group

    pennies = pennies / 1; // number of dimes contained in cents (quotient)
    cents = cents % 1; // cents remaining after pennies removed (remainder)

    System.out.print(pennies + "pennies, "); //done so that output is printed in a group








    }
}
