
//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab # 3
// Date: February 8, 2016

This assignment was very challenging as a new computer science student. I found that taking a few extra days past our lab time to complete it was the best idea because it gave me time to learn more during class lectures and through my reading about the basics that helped me to complete the assignment, i.e. variables, assignments, declarations, scanners, etc. This lab may have been very helpful in helping a customer to determine the approximate tip amount that would be warranted when dining at a restaurant without the embarrassment of using a calculator on one's phone or discussing it with the other individuals dining with the customer, for fear that the waitor might overhear the conversation. I had a difficult time understanding what the purposes of each statement, variable, etc. were until discussing them in class, reading more, troubleshooting myself, and getting assistance from other computer science students and/or TA's when I was simply stuck and confused. 

winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ javac TipCalculator.java
winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ java TipCalculator
Lauren Winterberg
 Lab #3
Mon Feb 08 12:44:49 EST 2016

Please enter your name:
Lauren
Lauren, Welcome to the tip calculator!
Please enter the amount of your bill:
89.53 
Please enter the percentage that you want to tip:
15
Your original bill was 89.53
Your tip amount is 13.429499999999999
Your total bill is 102.9595
How many people will be splitting the bill?
2
Each person should pay 51.47975
Have a nice day! You're great!
winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ 


winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ javac TipCalculator.java
winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ java TipCalculator
Lauren Winterberg
 Lab #3
Mon Feb 08 13:01:15 EST 2016

Please enter your name:
Lauren
Lauren, Welcome to the tip calculator!
Please enter the amount of your bill:
20.13
Please enter the percentage that you want to tip:
15
Your original bill was 20.13
Your tip amount is 3.0195
Your total bill is 23.1495
How many people will be splitting the bill?
5
Each person should pay 4.6299
Have a nice day! You're great!
winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ 




winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ javac TipCalculator.java
winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ java TipCalculator
Lauren Winterberg
 Lab #3
Mon Feb 08 13:02:18 EST 2016

Please enter your name:
Lauren
Lauren, Welcome to the tip calculator!
Please enter the amount of your bill:
150.43
Please enter the percentage that you want to tip:
20
Your original bill was 150.43
Your tip amount is 30.086000000000002
Your total bill is 180.51600000000002
How many people will be splitting the bill?
3
Each person should pay 60.172000000000004
Have a nice day! You're great!
winterbergl@aldenv180:~/cs111s2016/cs111s2016-winterbergl/labs/lab3$ 






