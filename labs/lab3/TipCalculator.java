
//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab # 3
// Date: February 8, 2016 
//
// Purpose: Providing satisfaction for a customer by helping to calculate the tip on a bill.
//*************************************
import java.util.Date;
import java.util.Scanner; 

public class TipCalculator
{
    //----------------------------
    // main method: program execution begins here
    //----------------------------
    public static void main (String[] args)
    {
          //Label output with name and date:
          System.out.println("Lauren Winterberg\n Lab #3\n" + new Date() + "\n");
          
	  String name; 
	  double bill;
	  double percent; 
	  double tip; 
	  double tbill;
	  int people = 0;
	  double eachperson;
	  
	  
	  System.out.println("Please enter your name:"); //First Statement to Customer
	  
	  Scanner input = new Scanner( System.in );
	  name = input.nextLine();
          
	  System.out.println(name + ", Welcome to the tip calculator!"); // First Statement

	  System.out.println("Please enter the amount of your bill:");  //Total Bill amount
	  bill = input.nextDouble(); 
	
	  System.out.println("Please enter the percentage that you want to tip:"); //How great was the waitor?
	  percent = input.nextDouble();

	  System.out.println("Your original bill was " + bill);  //Without the tip                                                
          tip = (percent/100) * bill;

	  System.out.println("Your tip amount is "  + tip); //Total Tip based on service
 	  tbill = tip + bill;
	  System.out.println("Your total bill is " + tbill); //The Whole Bill

	  System.out.println("How many people will be splitting the bill?"); //How many people are paying?
          people = input.nextInt();
           
	  eachperson = tbill/people;
	  System.out.println("Each person should pay " + eachperson); //How much should each person pay? Divide bill by number of people

	  System.out.println("Have a nice day! You're great!"); //Final Statement to customer
         
    }
} 
