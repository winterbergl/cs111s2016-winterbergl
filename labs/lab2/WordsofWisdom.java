
//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab # 2
// Date: January 28, 2016
//
// Purpose: Wise words for the day to learn programming
//*************************************
import java.util.Date; // needed for printing today's date

public class WordsofWisdom
{
    //----------------------------
    // main method: program execution begins here
    //----------------------------
    public static void main (String[] args)
    {
          //Label output with name and date:
          System.out.println("Lauren Winterberg\n Lab #2\n" + new Date() + "\n"); // lalalalalala
          System.out.println("Nothing is impossible. The word itself says I'm possible. To that I say, thank you Audrey."); // I love Audrey!
          System.out.println("The sun will come out tomorrow. Annie"); // My past explained.
    }
} 
