//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab # 8
// Date: April 6, 2016
//
// Purpose: To create a carpet pattern
//*************************************
import java.util.Date; // needed for printing today's date

import java.util.Scanner; //import Scanner

public class CarpetDesign

{


    public static void main(String[] args)  {

        System.out.println("Lauren Winterberg\n Lab #8\n" + new Date() + "\n");

       System.out.println("Enter a design character");  //prompt the user to input a character
       Scanner input = new Scanner(System.in);  //declare a instance of the scanner
       char design = input.next().charAt(0);   //assign the character "design"

       for(int i=0; i<2; i++)  //for loop to begin declaring design layout of first pattern
        {
            for(int j=0; j<23; j++)  //nested loop
            {
                System.out.print(design);
            }
            System.out.println();

        }

           for(int i=0; i<2; i++)
        {
            for(int j=0; j<46; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

           for(int i=0; i<2; i++)
        {
            for(int j=0; j<72; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

           for(int i=0; i<2; i++)
        {
            for(int j=0; j<72; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }


             for(int i=0; i<2; i++)
        {
            for(int j=0; j<46; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

               for(int i=0; i<2; i++)
        {
            for(int j=0; j<23; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

               for(int i=0; i<2; i++)  //for loop to create space before next design
        {
            for(int j=0; j<0; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

        for(int i=0; i<2; i++)  //start next design prompt
        {
            for(int j=0; j<11; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

            for(int i=0; i<2; i++)
        {
            for(int j=0; j<1; j++)
            {
                System.out.print("          "+design);
            }
            System.out.println();

        }


            for(int i=0; i<2; i++)
        {
            for(int j=0; j<11; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }


            for(int i=0; i<2; i++)  //create space before next design
        {
            for(int j=0; j<0; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }


            for(int i=0; i<2; i++)  //start next design
        {
            for(int j=0; j<22; j++)
            {
                System.out.print(design+"          ");
            }
            System.out.println();

        }



            for(int i=0; i<2; i++)
        {
            for(int j=0; j<22; j++)
            {
                System.out.print(design+"          ");
            }
            System.out.println();

        }

            for(int i=0; i<2; i++)  //create space before next design
        {
            for(int j=0; j<0; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

        for(int i=0; i<2; i++)  //start next design
        {
            for(int j=0; j<11; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

            for(int i=0; i<2; i++)
        {
            for(int j=0; j<1; j++)
            {
                System.out.print("          "+design);
            }
            System.out.println();

        }


            for(int i=0; i<2; i++)
        {
            for(int j=0; j<11; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }


            for(int i=0; i<2; i++)  //create space before next design
        {
            for(int j=0; j<0; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }


        for(int i=0; i<2; i++)
        {
            for(int j=0; j<23; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

           for(int i=0; i<2; i++)
        {
            for(int j=0; j<46; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

           for(int i=0; i<2; i++)
        {
            for(int j=0; j<72; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

           for(int i=0; i<2; i++)
        {
            for(int j=0; j<72; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }


             for(int i=0; i<2; i++)
        {
            for(int j=0; j<46; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

               for(int i=0; i<2; i++)
        {
            for(int j=0; j<23; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

        for(int i=0; i<2; i++)  //create space after last design
        {
            for(int j=0; j<0; j++)
            {
                System.out.print(design);
            }
            System.out.println();

        }

        System.out.println("What do you think of your Sideways NYC Skyline Carpet?"); //persuade the user to purchase carpet!
        System.out.println("25% off for first time buyers!");
    }
}
