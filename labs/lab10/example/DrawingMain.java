//==========================================
// Janyl Jumadinova
// Lab 10 Example
//
// Purpose: This program sets up a window with a "drawing
// canvas". To add things to the drawing, you must
// edit the file "DrawingCanvas.java".
//==========================================

import java.awt.*;
import javax.swing.*;

public class DrawingMain extends JApplet
{
    public static void main(String[] args)
    {
        	JFrame window = new JFrame("Janyl Jumadinova ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new DrawingMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
			window.setSize(600, 400);

        	//window.pack();
    }

    public void paint(Graphics page)
    {
        final int WIDTH = 600;
        final int HEIGHT = 400;

        //fill entire background cyan
        page.setColor(Color.cyan);
        page.fillRect(0,0,WIDTH, HEIGHT);

		DrawingCanvas d = new DrawingCanvas(page);
        d.drawSnow();
        d.drawSun();
        d.drawTree();
    }
}

