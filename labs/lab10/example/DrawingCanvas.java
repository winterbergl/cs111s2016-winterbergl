//=================================================
// Lab 10 Example using Graphics

// Purpose: Program draws a tree and a sun
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class DrawingCanvas extends JApplet
{
	Graphics page;

	// Constructor
	public DrawingCanvas(Graphics p)
	{
		page = p;
	}
    //-------------------------------------------------
    // Add content to the drawing canvas
    //-------------------------------------------------

    public void drawSnow()
    {

		//change color for the snow
		page.setColor(Color.white);

        //draw snow on the ground
        page.fillRect(0,300,600,80);
	}

	public void drawSun()
    {
        //draw sun in corner
        page.setColor(Color.yellow);
        page.fillOval(0,0,50,50);

        //draw rays from sun
        page.drawLine(70,30,100,50);
        page.drawLine(30,70,50,100);
        page.drawLine(50,50,100,100);
		page.drawLine(70,10,150,15);
		page.drawLine(10,65,6,130);
	}
	public void drawTree()
    {
		// draw trunk of the tree
		page.setColor(Color.gray);
		page.fillRect(432,200,30,160);

		// overlapping green circles for leaves
		page.setColor(Color.green); //custom dark green
		page.fillOval(410,100,125,125); // top right
		page.fillOval(362,130,117,117); // bottom left
		page.fillOval(360,100,125,125); //top left
		page.fillOval(412,130,117,117); // bottom right
		page.fillOval(385,90,117,117); // top center

		// black hole in tree trunk
		page.setColor(Color.black);
		page.fillOval(435,270,15,25);
	}
}
