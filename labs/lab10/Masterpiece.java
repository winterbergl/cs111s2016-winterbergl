//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab 11
// Date: 04 19 2016
//
// Purpose: To draw an image of an asteroid hitting the Earth in the middle of a farm in the middle of the night
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{
    Graphics page;



    // constructor
    public Masterpiece(Graphics p)
    {
        page = p;
    }

    //Draw pieces of the asteroid trailing behind
    public void drawAsteroidpieces()
    {
         page.setColor(Color.white);

         page.fillOval(10,10,10,10);
         page.fillOval(40,40,10,10);
         page.fillOval(80,80,10,10);
         page.fillOval(120,120,10,10);
         page.fillOval(160,160,10,10);
         page.fillOval(200,200,10,10);
         page.fillOval(240,240,10,10);

    }

    // Draw Grass
    public void drawGrass()
    {
        page.setColor(Color.green);
        page.fillRect(0,300,600,100);
    }

    //Draw Asteroid hitting the Earth
    public void drawAsteroid()
    {
         page.setColor(Color.orange);
         page.fillOval(250,265,35,35);

         page.drawLine(255,255,10,10);

    }

    //Draw Barn/Farmhouse
    public void drawHouse()
    {
         page.setColor(Color.black);
         page.fillRect(425,250,90,90);
    }

    //Draw roof of the barn/farmhouse
    public void drawRoof()
    {
        page.setColor(Color.red);
        page.fillRect(415,250,110,35);
    }

    //Draw the Moon
    public void drawMoon()
    {
        page.setColor(Color.white);
        page.fillOval(500,50,50,50);
        page.setColor(Color.blue);
        page.fillOval(525,50,25,25);
    }
}
