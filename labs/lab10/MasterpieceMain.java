//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.

// Lauren Winterberg
// CMPSC 111 Spring 2016
// Lab 11
// Date: 04 19 2016
//
// Purpose: To draw an image of an asteroid hitting the Earth in the middle of a farm in the middle of the night
//=================================================
import java.awt.*;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
import java.awt.image.BufferedImage;

public class MasterpieceMain extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {
        final int WIDTH = 600;
        final int HEIGHT =400;

        //To establish the dimensions of the window

        //Fill background with blue color with blue night sky look
        page.setColor(Color.blue);
        page.fillRect(0,0,WIDTH,HEIGHT);


        Masterpiece m = new Masterpiece(page);
        m.drawAsteroidpieces();
        m.drawGrass();
        m.drawAsteroid();
        m.drawHouse();
        m.drawRoof();
        m.drawMoon();

        //CALL METHODS


	// create an instance of Masterpiece class
	// NOTE: you may have to pass 'page' in addition to any
	//       other arguments in your constructor or during your method calls

	// call the methods in the Masterpiece class


    }

    // main method that runs the program
    public static void main(String[] args)
    {
        JFrame window = new JFrame(" Lauren Winterberg ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new MasterpieceMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		    window.setSize(600, 400);

    }
}
