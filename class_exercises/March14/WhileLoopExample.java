//==========================================
// Class Example
// While loop
// March 14, 2015
//
// Purpose: This program demonstrates the usage of while loop.
// The program keeps count of user input and exits after user enters five numbers.
//==========================================

import java.util.Scanner;
import java.util.Random;

public class WhileLoopExample
{
    public static void main ( String args[] )
    {

        Scanner scan = new Scanner ( System.in);
        Random rand = new Random();

       System.out.println(" Please enter five numbers.  ");
        int number;
        int count = 0;

        while(count<5)
        {
            number = scan.nextInt();
            System.out.println(" Number "+(count+1)+" is :: " +number);
            count++;
        }
        System.out.println("Ok, we received your five numbers. Thank you!");

        System.out.println("Enter a positive number: ");
        int number = scan.nextInt();
        while(number<0)
        {
             System.out.println("Enter a positive value");
             number = scan.nextInt();
             System.out.println("You entered: "+number);

        }

        int number = 1;
        while(number<0)
        {
             //print
            number+=2;
        }
     }
}

