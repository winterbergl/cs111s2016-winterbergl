//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Class Exercise
// Date: February 3, 2016
//
// Purpose: use variables and expressions, learn to use Scanner class
//*************************************
import java.util.Date; // needed for printing today’s date
import java.util.Scanner;

public class AgeStats
{
	//----------------------------
	// main method: program execution begins here
	//----------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Lauren Winterberg\n " + new Date() + "\n");
		
	        double age; // declaration
                age = 10; // assignment statement
                double result = 0; // declaration and assignment in one statement
              
                // declare a Scanner object
                Scanner userInput = new Scanner (System.in); 

                System.out.println("Enter an age"); 
                age = userInput.nextInt(); 

                // convert age to centuries
                result = age*10/100;
                System.out.println("Age in centuries"  +result);
		
	}
}
