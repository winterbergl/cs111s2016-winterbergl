//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Janyl Jumadinova
// CMPSC 111 Spring 2016
// Class Example Program
// Date: February 8, 2016
//
// Purpose: Program asking for input from the user
//*************************************
import java.util.Scanner;

public class ScannerStringExample
{
	public static void main(String[] args)

	{

    	// Declarations

    	Scanner in = new Scanner(System.in);

    	String string1;
    	String string2;
		int number;

      	// Prompts
    	System.out.println("Enter your favourite food");
    	string1 = in.nextLine();
    	// note if this is changed to next(), it will skip to the last input
    	// to fix it you can change next() to nextLine(), or add an additional nextLine() statement

    	System.out.println("Enter your favourite hobby");
    	string2 = in.nextLine();

    	System.out.println("Enter your favourite number");
		number = in.nextInt();

    	System.out.println("Here is what you entered: "+string1+" and "+string2+" and "+number);
	}
}
