
//*************************************
// Honor Code: This work is mine unless otherwise cited.
// 
// CMPSC 111 Spring 2016
// 
// Date: Feb 10 2016
//
// Purpose: To convert temperature from Farenheits to Celcius
//*************************************
import java.util.Date;
import java.util.Scanner;
public class TempConversion 
{
	//----------------------------
	// main method: program execution begins here
	//----------------------------
	public static void main(String[] args)
	{
		// Label output with name and date:
		System.out.println("Janyl Jumadinova\n " + new Date() + "\n");
		Scanner userInput = new Scanner(System.in);
		float tempFarenheit;
		float tempCelcius;
		float tempChange;
		
		System.out.println("Please enter temperature in Farenheits");
		tempFarenheit = userInput.nextFloat(); // read in input and save it as a float data type
		tempCelcius = (tempFarenheit-32)*(float)5/9; // casting 5 to become a float data type
		System.out.println("The temperature in Celcius is : " + tempCelcius);

		
		
		// TO DO: ask the user to enter temperature change in F
		// Modify the result based on that change

		System.out.println("Please enter a temperature change");
		tempChange = userInput.nextFloat();
		tempFarenheit += +tempChange;
		System.out.println("Temp F "+tempFarenheit);
		tempCelcius = (tempFarenheit-32)*(float)5/9; // casting 5 to become a float data type
		System.out.println("The temperature in Celcius is : " + tempCelcius);


	}
}
