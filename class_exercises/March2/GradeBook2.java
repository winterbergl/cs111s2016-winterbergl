// Parameters?

public class GradeBook2 {
    // method to display a welcome message
    public void displayMessage(String courseName) { //Parameters are the courseName type String
        System.out.println("Welcome to the Grade Book for "+courseName);  //welcome with specific course name (parameters)
    }
}
