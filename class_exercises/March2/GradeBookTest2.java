
import java.util.Scanner; //Scanner is used to allow user to enter the course name


public class GradeBookTest2 {
    public static void main (String args[]) {
        Scanner input = new Scanner (System.in);
        GradeBook2 myGradeBook = new GradeBook2();

        System.out.println("Please enter the course name:");
        String courseName = input.nextLine(); //save as a variable courseName to use in other program
        System.out.println();

        myGradeBook.displayMessage(courseName); //sent courseName to gradebook2 line with corresponding variable

    }
}
