// Identify all of the methods (parameters, return types)

public class GradeBook3 {
    private String courseName = "Music101"; //instance variable
    //changing or modifying the courseName  -  setter
    public void setCourseName (String name) {
        courseName = name;
    }
    //returning the String to the variable CourseName  -  getter
    public String getCourseName () {
        return courseName;
    }

    public void displayMessage() {
        System.out.println("Welcome to the Grade Book for " +getCourseName());
    }
}
