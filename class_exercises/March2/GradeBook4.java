// Identify all methods and their functionalities

public class GradeBook4 {
//instance variable
    private String courseName;


    //constructor
    public GradeBook4 (String name)
    {
        courseName = name;
    }

    public void setCourseName (String name) {
        courseName = name;

        System.out.println("Inside setCourseName");

    }

    public String getCourseName () {
        System.out.println("Inside getCourseName");
        return courseName;
    }

    public void displayMessage() {
        System.out.println("Welcome to the Grade Book for " +getCourseName());
    }
}
