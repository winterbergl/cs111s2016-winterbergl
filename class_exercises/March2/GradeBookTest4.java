// GradeBookTest class with argument

import java.util.Scanner;

public class GradeBookTest4 {
    public static void main (String args[]) {
        GradeBook4 gb1 = new GradeBook4 ( "CMPSC 111 : Intro to Computer Science I" );
        GradeBook4 gb2 = new GradeBook4 ( "CMPSC 112 : Intro to Computer Science I" );
        System.out.println ("gb1 course name is: "+gb1.getCourseName() );
        System.out.println ("gb2 course name is: "+gb2.getCourseName() );

        gb1.setCourseName("blah");
        gb2.setCourseName("blah,blah");
        System.out.println ("gb1 course name is: "+gb1.getCourseName() );
        System.out.println ("gb2 course name is: "+gb2.getCourseName() );


    }
}
