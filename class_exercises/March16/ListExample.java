//March 16, 2016
//
//Lauren Winterberg
//
import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.IOExemption;

public class ListExample
{
    public static void main (String args[]) throws IOException
    {
        ArrayList<String> twitterWords = new ArrayList<String>();

        File file = new File("words.txt");
        Scanner input = new Scanner(file);

        while(input.hasNext())
        {
            String word = input.next();
            System.out.println("Processing word "+word);
            //add the word to the list
            twitterWords.add(word);
        }
        //display all words
        System.out.println(twitterWords);
        System.out.println("Size: "+twitterWords.size());

        //add an element to the list
        twitterWords.add("sofetch");

        //remove all words ending in s
        int count =0;
        //go through the list
        while(count < twitterWords.size())
        {
            String word = twitterWords.get(count);
            if(word.endsWith("s"))
            {
                twitterWords.remove(count);
            }
            count++;
        }

    }
}
