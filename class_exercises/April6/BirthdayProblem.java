import java.io.*;
import java.util.Scanner;

public class BirthdayProblem
{
    public static void main(String args[]) throws IOException
    {
        String [] birthdays = new String [32];
        String [] seen = new String [32];

        // read a file and save it into birthdays array
        File file = new File("birthdays.txt");
        Scanner in = new Scanner(file);

        int count = 0;
        while(in.hasNext())
        {
            // read one birthday
            String birthday = in.next();
            // add to the array
            birthdays[count] = birthday;
            System.out.println(birthdays[count]);
            count++;
            // lines 11-24 = have a text file read one birthday and putting it into one slot and so on

        }

        for(int i=0; i<birthdays.length; i++)
        {
             for(int j=0; j<seen.length; j++)
             {
                 if(birthdays[i].equals(seen[j]))
                 {
                     System.out.println("Duplicate birthday"+birthdays[i]);
                     break;
                 }
             }
             seen[i]=birthdays[i];
        }
    }
}
