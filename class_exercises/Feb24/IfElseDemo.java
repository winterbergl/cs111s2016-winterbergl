import java.util.Scanner;

public class IfElseDEmo
{
    public static void main (String args[]);
    {
        Scanner input = new Scanner(System.in);
        char character;

        System.out.println("Enter a character: ");
        character = input.next().charAt(0);
        if(character =='a')
        {
            System.out.println(character+" is a vowel");

        }
        else if (character=='e')
        {
             System.out.println(character+" is a vowel");

        }
        else if (character=='i')
        {
             System.out.println(character+" is a vowel");

        }
        else if (character=='o')
        {
            System.out.println(character+" is a vowel");

        }
        else if (character=='u')
        {
            System.out.println(character+" is a vowel");

        }
        else
        {
            System.out.println(character+" is a vowel");

        }
    }
}
