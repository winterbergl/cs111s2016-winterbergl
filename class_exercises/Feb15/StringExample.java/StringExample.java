public class String Example
{
    public static void main (String args[])
    {
        String word = "something";
        String word1 = new String ("something else");
        System.out.println(word+", "+word1);

        char letter = word.charAt(0);
        System.out.println("Letter: "+letter);
        System.out.println("Length of word1: "+word1.length());

        String word2 = word1.substring(10);
        System.out.println("Substring of word1: "+word2);

        String word3 = word1.substring(4,9);
        System.out.println ("Another substring of word1: "+word3);

        System.out.println("Equality:" "+word.equals(word1));

        System.out.println("Replace: "word1.replace('e' , 'E'));
    }
}
