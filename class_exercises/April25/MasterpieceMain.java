//=================================================
// Honor Code: The work I am submitting is 
// a result of my own thinking and efforts.

// Your Name [Replace with your name]
// CMPSC 111 Fall 2015
// Lab 11
// Date: mmm dd yyyy [fill in the date]
//
// Purpose: ... [describe the program]
//=================================================
import java.awt.*;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
import java.awt.image.BufferedImage;

public class MasterpieceMain extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {

	// create an instance of Masterpiece class
	// NOTE: you may have to pass 'page' in addition to any 
	//       other arguments in your constructor or during your method calls

	// call the methods in the Masterpiece class

BufferedImage img = null;
try {
    img = ImageIO.read(new File("index.jpeg"));
} catch (IOException e) {
}
       

try {
    URL url = new URL(getCodeBase(), "index.jpeg");
    img = ImageIO.read(url);
} catch (IOException e) {
}

    }

    // main method that runs the program
    public static void main(String[] args)
    {
        	JFrame window = new JFrame(" Your name ");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new MasterpieceMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);
        	
    }
}
