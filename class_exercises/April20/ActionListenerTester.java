/**
 * Simple class to test an ActionListener
 * Uses the class from SetTextListener.java
 */

import javax.swing.*;
import java.awt.*;

public class ActionListenerTester {
	public static void main(String [] args)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new FlowLayout());

		JButton myButton = new JButton("I'm a JButton");
		JTextField myTextField = new JTextField("I'm a JTextField");
		JLabel myLabel = new JLabel("I'm a JLabel");

		myButton.addActionListener(new SetTextListener(myTextField, myLabel));

		frame.add(myButton);
		frame.add(myTextField);
		frame.add(myLabel);

		frame.pack();
		frame.setVisible(true);
	}
}
