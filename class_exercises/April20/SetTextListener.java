/**
 * This ActionListener object sets a JLabel to a textField's contents
 *
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class SetTextListener implements ActionListener {

	public SetTextListener(JTextField textField, JLabel label)
	{
		myLabel = label;
		myTextField = textField;
	}

	public void actionPerformed(ActionEvent event)
	{
		myLabel.setText(myTextField.getText());
	}

	private JLabel myLabel;
	private JTextField myTextField;
}
