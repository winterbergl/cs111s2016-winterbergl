import java.awt.*;
import javax.swing.*;

/**
 * Simple class to show how JPanels can be treated as JComponents
 *
 */
public class JPanelTester {

	public static void main(String [] args)
	{
		JFrame frame1 = new JFrame();
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame1.setLayout(new FlowLayout());

		frame1.add(new JButton("JComponents added"));
		frame1.add(new JLabel("to this JFrame"));
		frame1.add(new JTextField("are laid out"));
		frame1.add(new JButton("by FlowLayout"));

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0,1));

		panel.add(new JButton("JComponents added"));
		panel.add(new JLabel("to this JPanel"));
		panel.add(new JTextField("are laid out"));
		panel.add(new JButton("by GridLayout"));

		frame1.add(panel);

		frame1.pack();
		frame1.setVisible(true);
	}
}
