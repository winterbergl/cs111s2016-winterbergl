import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;

public class ChangeListenerTester {

	public static void main(String [] args)
	{
		JProgressBar myBar = new JProgressBar();
		myBar.setValue(50);

		JSlider mySlider = new JSlider();
		mySlider.addChangeListener(new MyListener(myBar));

		JFrame frame = new JFrame("ChangeListener Tester");
		frame.setLayout(new FlowLayout());
		frame.add(myBar);
		frame.add(mySlider);
		frame.pack();
		frame.setVisible(true);
	}

	public static class MyListener implements ChangeListener
	{
		public MyListener(JProgressBar bar)
		{
			myProgressBar = bar;
		}

		public void stateChanged(ChangeEvent event)
		{
			JSlider mySlider = (JSlider)event.getSource();

			myProgressBar.setValue(mySlider.getValue());
		}

		private JProgressBar myProgressBar;
	}
}
