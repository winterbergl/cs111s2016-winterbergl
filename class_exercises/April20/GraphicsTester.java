/**
 * A simple class to experiment with Swing graphics
 */

import javax.swing.*;
import java.awt.*;

public class GraphicsTester {
	public static void main(String [] args)
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setLayout(new FlowLayout());
		frame.add(new JButton("I'm a JButton"));
		frame.add(new JTextField("I'm a JTextField"));
		frame.add(new JLabel("I'm a JLabel"));
		frame.add(new JSlider());
		frame.add(new JProgressBar());

		frame.pack();
		frame.setVisible(true);
	}
}
