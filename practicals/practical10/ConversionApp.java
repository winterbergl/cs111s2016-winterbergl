//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg
// CMPSC 111 Spring 2016
// Practical #10
// Date: 04 22 2016
//
// Purpose: To create an app to convert time from seconds to minutes
//*************************************

import javax.swing.*;
import java.awt.*;

public class ConversionApp
{
    public static void main (String [] args)
    {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new FlowLayout());

        JButton myButton = JButton("I'm a JButton");
        JTextfield myTextField = new JTextField("I'm a JTextField");
        JLabel myLabel = new JLabel("I'm a JLabel");

        myButton.addConversionAppTwo(new ConversionAppTwo(myTextField, myLabel));
    }
}
