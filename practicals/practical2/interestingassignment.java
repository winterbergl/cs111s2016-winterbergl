//**********************************
// Lauren Winterberg
// Practical 2
// 29 January 2016
// Prints a name 'Bob'
// Honor Code: This work is mine unless otherwise cited. 
//**********************************
import java.util.Date;
public class interestingassignment
{
  public static void main(String[] ARGS)
  {
     System.out.println("Lauren Winterberg, CMPSC 111\n" + new Date() + "\n");
     System.out.println(" |    |  ____  |       /----- \\  ");
     System.out.println(" |    | |      |       |      |   ");
     System.out.println(" |    | |      |       |      |   ");
     System.out.println(" |____| |____  |       |____ /    ");
     System.out.println(" |    | |      |       |          ");
     System.out.println(" |    | |      |       |          ");
     System.out.println(" |    | |____  |_____  |          ");
   }
}

