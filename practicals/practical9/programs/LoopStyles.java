//*********************************************************************************
// CMPSC 111 Spring 2016
// Practical 9
//
// Purpose: Program demonstrating usage of the do while and for loops with the Iterator class
//*********************************************************************************
import java.util.*;

public final class LoopStyles {

    public static void main(String[] args) {
        ArrayList<String> flavours = new ArrayList<String>();
        flavours.add("chocolate");
        flavours.add("strawberry");
        flavours.add("vanilla");
        flavours.add("mint");
        flavours.add("cookies and cream");
        flavours.add("peanut butter");
        flavours.add("caramel");
        flavours.add("chocolate chip");
        flavours.add("pistachio");
        flavours.add("pecan");

        useDoWhileLoop(flavours);

        useForLoop(flavours);
    }

    private static void useDoWhileLoop(ArrayList<String> aFlavours) {
        Iterator<String> flavoursIter = aFlavours.iterator();
	// TO DO: rewrite this loop as a do..while loop
    do
    {
        System.out.print(flavoursIter.next());
        System.out.println();
    }
        while (flavoursIter.hasNext());

        }


    /**
     * Note that this for-loop does not use an integer index.
     */
    private static void useForLoop(ArrayList<String> aFlavours) {
        for (Iterator<String> flavoursIter = aFlavours.iterator(); flavoursIter.hasNext();){
            System.out.println(flavoursIter);
            break;
        }
    }
}

