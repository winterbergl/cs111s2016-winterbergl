//*********************************************************************************
// CMPSC 111 Spring 2016
// Practical 9
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        String day = s.nextString();


    // TO DO: Using a switch statement, given the day of the week, print whether it is a weekday or a weekend day.
    }
}
