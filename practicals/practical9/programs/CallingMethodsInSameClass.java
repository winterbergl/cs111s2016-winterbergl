//*********************************************************************************
// CMPSC 111 Spring 2016
// Practical 9
//
// Purpose: Program demonstrating  illustrates how to call static methods a class
// from a method in the same class. Static methods are the methods that can be called
// without creating an instance of the object.
//*********************************************************************************

public class CallingMethodsInSameClass
{
    public static void main(String args[])
    {
        printOne();
        printTwo();
        printThree();
        // TO DO: add printThree() method call
    }

    public static void printOne()
    {
        System.out.println("Nothing is impossible, the word itself says \"I'm possible\" !");
    }

    public static void printTwo()
    {
        printOne();
    }

    public static void printThree()
    {
        printTwo();
    }

    // TO DO: write a new method called printThree() that calls method printTwo() one time
}
