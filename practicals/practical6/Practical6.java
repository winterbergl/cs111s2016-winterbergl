//====================================
// Lauren Winterberg
// CMPSC 111
// Practical 6
// 4 March 2016
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Lauren Winterberg\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Utensil spat;           // a kitchen utensil
        Octopus ockyy;          // declare new octopus
        Utensil wisk;           // declare new utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky", 10);    // create and name the octopus and set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil



        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());


        ockyy = new Octopus("Ockyy", 35);  // create and name new octopus and set the age
        ockyy.setWeight(600);  //... weight...
        ockyy.setUtensil(wisk);   // ... and favorite utensil


        wisk = new Utensil("wisk"); // create a wisk
        wisk.setColor("blue"); // set spoon properties--color...
        wisk.setCost(4.36);   // ...and price

        System.out.println(ockyy.getName() + " weighs " +ockyy.getWeight()
                + " pounds\n" + " and is " +ockyy.getAge()
                + " years old. His favorite utensil is a " + ockyy.getUtensil());

        System.out.println(ockyy.getName() + "'s " + ockyy.getUtensil() + " costs $"
            + ockyy.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());



    }
}
