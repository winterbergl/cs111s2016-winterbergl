/************************************
  Honor Code: This work is mine unless otherwise cited.
  Lauren Winterberg
  CMPSC 111
  19 February 2016
  Practical 4

  Basic Input with a dialog box
 ************************************/

import javax.swing.JOptionPane;
import java.util.Random;
public class Practical4
{
    public static void main ( String[] args )
    {

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "Let's get you a new identity!" );

        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog(" What is your first name?");

        //create a message with the modified first name
        String newName = "Your new first name is "+ name+"ka";

        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName);

        // TO DO: prompt the user to get the last name, modify the last name and display the new last name
        String lastname = JOptionPane.showInputDialog("What is your last name?");

        String newlastname = "Your new last name is " +lastname+ "ka";
        JOptionPane.showMessageDialog(null, newlastname);

        // prompt user to enter the age
        int ans = Integer.parseInt(JOptionPane.showInputDialog("Enter your age"));

        Random r = new Random();  //importing random
        int i;     // declare integer
        i = r.nextInt();        // define variable as random



        // modify the age

        String newAge = "Your age is: "+(ans + 10); // TO DO: change age using a random number, instead of a constant 10

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        // to do: come up with your own (at least two) questions and answers

        // prompt the user to enter the name the name of his/her high school
        String school = JOptionPane.showInputDialog("What is the name of your high school?");

        // display the name of the user's new high school
        String newschool = "Your new high school is Allegany High School.";
        JOptionPane.showMessageDialog(null, newschool);

       // prompt the user to enter the name of his/her current sibling
        String sibling = JOptionPane.showInputDialog("What is the name of your sibling?");

        // display the new name of the user's sibling
        String newsibling = "Your sibling's name is Juan Suarez.";
        JOptionPane.showMessageDialog(null, newsibling);

       // Greet and congratulate the user, and wish them well
        JOptionPane.showMessageDialog( null, "Congratulations on your new identity! Use your power for good!" );



    } //end main
}  //end class Practical4
