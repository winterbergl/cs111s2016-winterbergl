//import all of the classes needed for this class
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TaskList1
{
	String job;

    //Creates a instance variable called ''/ This variable is an Array list, whose elements are of type TodoItem
    private ArrayList<Task> tasks;

    //Creates a non editable string called TODOFILE which holds the name of the file we want to take input from
    private static final String TASKLISTFILE = "TaskList.txt";

    //Constructor for this object, takes no parameters and initializes the 'todoitems' Array list
    public TaskList1()
    {
        tasks = new ArrayList<Task>();
    }

    //A class method that does not return anything. It adds a TodoItem object into the todoItems ArrayList
    public void addTask(Task task)
    {
        tasks.add(task);
    }

    //A class method called 'getTodoItems' that returns an iterator object from the todoItems Array List
    public Iterator getTasks()
    {
        return tasks.iterator();
    }

    //A class method that reads in todoItems from a text file, takes no parameters
    public void readTasksFromFile() throws IOException
    {
        //Creates a new scanner object that takes input from a file.
        //new File(TODOFILE) creates a new file, whose name is "TODOFILE" which is a class variable
        //that holds the name of our text file
        Scanner fileScanner = new Scanner(new File(TASKLISTFILE));

        //While loop that continues running as long as there is still input from the scanner object
        //Basically, continues running as long as there is still stuff to take from the text file
        while(fileScanner.hasNext())
        {
            //Create a string that is set equal to the next line in the text file
            String taskLine = fileScanner.nextLine();
            //Create another scanner that reads from the string we just created.
            //Basically, it reads the first line from the text file
            Scanner taskScanner = new Scanner(taskLine);
            //Sets the delimeter of the todoScanner object  as the "," character
            //This basically splits up the string, using the "," as "white space" to separate individual words
            //in a sentence.
            taskScanner.useDelimiter(",");
            //Declare three strings
            //String task;
            //Because of the delimiter, each string before ',' can be read using the next() method of a scanner
            //These statements set the three newly created strings as the individual
            //parts of the sentences in the "todo.txt".
            job = taskScanner.next();
            //This creates an object of type TodoItem and uses the constructor to make its variables equal
            //to the ones entered in the text file by passing the constructor the three strings we just created
            Task task = new Task(job);
            //Adds this newly created object to the todoItems ArrayList in this class
            tasks.add(task);
        }
    }

    //Method used to mark the task as done. Doesn't return anything but takes in an integer of the item to be
    //marked as completed
    public void markJobAsDone(int toMarkRank) {
        //Creates a new iterator object that is initialized as the iterator from the todoItems Array list
        //This iterator will have the elements of todoItems (which are TodoItems) as its input
        Iterator iterator = tasks.iterator();
        //A while loop that keeps running as long as the iterator we made, has elements to go through
        while(iterator.hasNext()) {
            //Creates an object of type TodoItem that is equal to the next element in the iterator we made
            Task task = (Task)iterator.next();
            //Checks if the ID of the next element is equal to the entered by the user
            //if it is, run the markDone method to set it as done
            if(task.getRank() == toMarkRank) {
                task.markDone();
            }
        }
    }

    //Outputs the todoItems array elements as a string
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = tasks.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}

