public class Task
{
    //Declare instance & class variables

     //ID for the object
    private int rank;
    //ID for the next object. Class variable
    private static int nextRank = 0;
    //Task for the object
    private String task;
    //Boolean value for whether object is complete
    private boolean done;
private String job;

    //Constructor for object. Parameters are three strings
    public Task(String j)
    {
        //Sets the instance variable 'task' equal to the 'p' string parameter entered
        job = j;
        //Sets the item as 'not done'
        done = false;
    }

    //Get Methods
    //
    public int getRank()
    {
        return rank;
    }
    public String getJob()
    {
        return job;
    }
    //Sets the task as 'done'
    public void markDone()
    {
        done = true;
    }
    //Checks if the task is marked as 'done'
    public boolean isDone()
    {
        return done;
    }
    //Returns a string that has the task and completeness of this specific task
    public String toString()
    {
        return new String(rank + ", " + job + ", done? " + done);
    }

}

