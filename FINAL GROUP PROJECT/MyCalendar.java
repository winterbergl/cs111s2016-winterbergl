//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg, Zarian Prenatt, Rayna Pelisari
// CMPSC 111 Spring 2016
// Group Project
// Date: April 21, 2016
//
// Purpose: Create a calendar that will remind user of daily events
//*************************************
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyCalendar extends JFrame
{

    //----------------------------
    // main method: program execution begins here
    //----------------------------
    public static void main (String[] args)
    {
		// SwingUtilities and run() let us call our methods
		SwingUtilities.invokeLater(new Runnable(){
    	    public void run() {
    	    	MyCalendar1 c = new MyCalendar1();
    	        c.displayJFrame();
    	    }
    	});
	} // main


} // class

