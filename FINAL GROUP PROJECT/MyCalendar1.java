//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Lauren Winterberg, Zarian Prenatt, Rayna Pelisari
// CMPSC 111 Spring 2016
// Group Project
// Date: April 21, 2016
//
// Purpose: Create a calendar that will remind user of daily events
//*************************************
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyCalendar1 extends JFrame
{
    static JFrame frame1;
    static String taskName="";
    static ArrayList<String> myList = new ArrayList<String>(); // creating an array list for the user's tasks they input


   static void displayJFrame()
    {

    // create our first frame, displays the current date and time at the top
    frame1 = new JFrame("MyCalendar: "+ new Date() + "\n");
    frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // displays layout of objects on frame from left to right
    frame1.setLayout(new GridLayout(0,1));

    // this label is shown first and tells the user to click one of the buttons that will be displayed
    JLabel Label1 = new JLabel("Welcome to My Calendar! Please choose an action.");

	// create the buttons on the first frame
    JButton Button1 = new JButton("Add Task");
    //JButton Button2 = new JButton("Remove Task");

    // adds the label & two buttons to the first frame
    frame1.add(Label1);
    frame1.add(Button1);
    //frame1.add(Button2);
    frame1.pack();
    frame1.setVisible(true);
    frame1.setSize(1000,500);

    // add the listener to the "add task" button to handle the "pressed" event
    Button1.addActionListener(new ActionListener()
    {
        public void actionPerformed(ActionEvent e)
        {

            JFrame frame2 = new JFrame("Tasks"); // creates a second frame
            frame2.setLayout(new FlowLayout()); // sets the layout of the second frame (left to right)
            frame2.setLocationRelativeTo(frame1); // frame 2 displays center of screen under "add task"

            Scanner scan = new Scanner(System.in); // creating/initializing scanner

            JLabel label1 = new JLabel("Insert a Task"); // create new label telling user to type in a task
            JTextField text1 = new JTextField("            "); // create a blank text box for user to type in
            JButton button2 = new JButton("Submit"); // create a button user can click to submit task
            JLabel label2 = new JLabel("  "); // creating a new label that will contain the user's tasks and be added to frame one

            // add a listener to the "submit" button to handle the "pressed" event
            button2.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent event)
                {
                    taskName = text1.getText(); // variable taskName is assigned the text inserted by user
                    myList.add(taskName); // tasks are added to the list
                    Utility u = new Utility(); // creating/initializing utility variable
                    String tasks = u.loop(myList); // putting the string tasks on a loop that will be run in "Utility" class
                    label2.setText(tasks); // setting the second label declared earlier to have the tasks inserted by user
                }
		    });

            frame2.add(button2); // adding button 2 to frame 2
            frame2.add(label1); // adding label 1 to frame 2
            frame2.add(text1); // adding text box 1 to frame 2
            frame1.add(label2); // adding label 2 to frame 1
            frame2.pack();
            frame2.setVisible(true);
            frame1.setBackground(Color.green);

        }
    });
    }
}
